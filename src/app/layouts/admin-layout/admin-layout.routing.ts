import { ExtendedsComponent } from './../../forms/extended.component';
import { NotificationComponent } from './../../component/notification.component';
import { WidgetsComponent } from './../../widgets/widgets.component';
import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { DataTableComponent } from '../../table-list/table-datatable-net.component';
import { ExtendedComponent } from '../../table-list/table-extended.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { CalenderComponent } from '../../calender/calender.component';
import { GridComponent } from '../../component/grid.component';
import {  ButtonComponent} from '../../component/button.component';
import { PanelComponent } from 'app/component/panel.component';
import { SwalComponent } from 'app/component/swal.component';
import { RegularComponent } from 'app/forms/regular.component';
import { ValidationComponent } from 'app/forms/validation.component';

export const AdminLayoutRoutes: Routes = [

    { path: 'dashboard',      component: DashboardComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'table/regular',     component: TableListComponent },
    { path: 'table/datatables.net',     component: DataTableComponent },
    { path: 'table/extended',     component:  ExtendedComponent},
    { path: 'typography',     component: TypographyComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent },
    { path: 'notifications',  component: NotificationsComponent },
    { path: 'upgrade',        component: UpgradeComponent },
    { path: 'widgets',        component: WidgetsComponent },
    { path: 'calender',        component: CalenderComponent },
    { path: 'components/grid',        component: GridComponent },
    { path: 'components/button',        component: ButtonComponent },
    { path: 'components/panel',        component: PanelComponent },
    { path: 'components/swal',        component: SwalComponent },
    { path: 'components/notification',        component: NotificationComponent },
    { path: 'forms/regular',        component: RegularComponent },
    { path: 'forms/extended',        component: ExtendedsComponent },
    { path: 'forms/validation',        component: ValidationComponent },
    
];
