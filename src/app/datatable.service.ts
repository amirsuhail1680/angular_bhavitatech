import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class DatatableService {
  debounceTime(arg0: number) {
    throw new Error("Method not implemented.");
  }

  constructor(private http:HttpClient) { }

  public getUsers() {
    return this.http.get<any>('https://reqres.in/api/users?page=2');
  }

}
