import { DatatableService } from './../datatable.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {
  userData=[];
  constructor(private data:DatatableService) { }

  ngOnInit() {
    this.getUserList();
  }
  getUserList() {

    return this.data.getUsers().subscribe(
      res => {
        if (res) {
          this.userData = res.data;          
        }
      },
      error => {
        alert(error);
      });
  }
}
