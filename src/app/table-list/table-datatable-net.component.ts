import { Component, OnInit } from '@angular/core';
declare let $: any; 
import { DatatableService } from "../datatable.service";
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-datatable-net-list',
  templateUrl: './table-datatable-net.component.html',
  styleUrls: ['./table-datatable-net.component.css']
})
export class DataTableComponent implements OnInit {
  userData=[];
  ad=[];
  userListForm:FormGroup;
  per_page: any;
  total: any;
  total_page: any;
  page: any;
  constructor(private data:DatatableService,private fb:FormBuilder) { }

  ngOnInit() {
    this.getUserList();
    // let table = $('#datatables').DataTable({
    //   drawCallback: () => {
    //     $('.paginate_button.next').on('click', () => {
    //         this.nextButtonClickEvent();
    //       });
    //   }
    // });
  }
  

  nextButtonClickEvent(): void {
    //do next particular records like  101 - 200 rows.
    //we are calling to api
    
    console.log('next clicked')
  }
  getUserList() {

    return this.data.getUsers().subscribe(
      res => {
        if (res) {
          this.ad=res.ad;
          this.per_page=res.per_page;
          this.total=res.total; 
          this.total_page=res.total_pages;
          this.page=res.page;
          this.userData = res.data; 
          console.log(res)         
        }
      },
      error => {
        alert(error);
      });
  }
  onClicF(alerts){
    alert('You clicked on Like button')
  }
  onClicDv(details){
alert('You press on Row:'+details.first_name + ' ' +details.last_name+ ' ' +"Design's row")
  }

  onClicCl(indexs){
    const index = this.userData.indexOf(indexs);
if (index > -1) {
  this.userData.splice(index, 1);
  this.total-1;
}
  }
  
}
