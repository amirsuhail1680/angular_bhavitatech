import { Component, OnInit } from '@angular/core';

declare const $: any;
// Start create Interface
declare interface RouteInfo {
  id: string;
  path: string;
  title: string;
  icon: string;
  class: string;
  isDropDownActive: boolean
  dropDownList
}
// End create Interface

// Start Export class For Routes
export const ROUTES: RouteInfo[] = [
  { id: '1', path: '/dashboard', title: 'Dashboard', icon: 'dashboard', class: '', isDropDownActive: false, dropDownList: [{}] },
  { id: '2', path: '/components', title: 'Components', icon: 'apps', class: '', isDropDownActive: true, dropDownList: [{ normal: 'Buttons', mini: 'B', path: '/button' }, { normal: 'Grid System', mini: 'GS', path: '/grid' }, { normal: 'Panel', mini: 'P', path: '/panel' }, { normal: 'Sweat Alert', mini: 'SA', path: '/swal' }, { normal: 'Notifications', mini: 'N', path: '/notification' }] },
  { id: '3', path: '/forms', title: 'Forms', icon: 'content_paste', class: '', isDropDownActive: true, dropDownList: [{ normal: 'Regular Forms', mini: 'RF', path: '/regular' }, { normal: 'Extended Forms', mini: 'EF', path: '/extended' }, { normal: 'Validation Forms', mini: 'VF', path: '/validation' }] },
  { id: '4', path: '/table', title: 'Table List', icon: 'grid_on', class: '', isDropDownActive: true, dropDownList: [{ normal: 'Regular Tables', mini: 'RT', path: '/regular' }, { normal: 'Extended Tables', mini: 'ET', path: '/extended' }, { normal: 'Datatables.net', mini: 'DT', path: '/datatables.net' },] },
  { id: '5', path: '/typography', title: 'Typography', icon: 'library_books', class: '', isDropDownActive: false, dropDownList: [{}] },
  { id: '6', path: '/icons', title: 'Icons', icon: 'bubble_chart', class: '', isDropDownActive: false, dropDownList: [{}] },
  { id: '7', path: '/maps', title: 'Maps', icon: 'location_on', class: '', isDropDownActive: false, dropDownList: [{}] },
  { id: '8', path: '/widgets', title: 'Widgets', icon: 'widgets', class: '', isDropDownActive: false, dropDownList: [{}] },
  { id: '9', path: '/notifications', title: 'Charts', icon: 'timeline', class: '', isDropDownActive: false, dropDownList: [{}] },
  { id: '10', path: '/calender', title: 'Calender', icon: 'date_range', class: '', isDropDownActive: false, dropDownList: [{}] },
  { id: '11', path: '/notificatio', title: 'Pages', icon: 'image', class: '', isDropDownActive: true, dropDownList: [{ normal: 'Pricing', mini: 'P', path: '' }, { normal: 'Timeline Page', mini: 'TP', path: '' }, { normal: 'Login Page', mini: 'LP', path: '' }, { normal: 'Register Page', mini: 'RP', path: '' }, { normal: 'Lock Screen Page', mini: 'LSP', path: '' }, { normal: 'User Page', mini: 'UP', path: '' }] },
  { id: '12', path: '/notificati', title: 'Documentation', icon: 'school', class: '', isDropDownActive: false, dropDownList: [{}] },

];
// End Export class For Routes

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };
  onClick(url) {
    console.log(url)
  }
}
